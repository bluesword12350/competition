import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner sc) {
        System.out.println(Math.round(sc.nextDouble()));
    }

}
