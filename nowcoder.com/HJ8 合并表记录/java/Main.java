import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner in) {
        Map<Integer,Integer> map = new TreeMap<>(Integer::compareTo);
        int length = Integer.parseInt(in.nextLine());
        for (int i = 0; i < length; i++) {
            String[] strings = in.nextLine().split(" ");
            int key = Integer.parseInt(strings[0]);
            int value = Integer.parseInt(strings[1]);
            if (map.containsKey(key)) {
                map.put(key,map.get(key)+value);
            } else {
                map.put(key,value);
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
    }

}