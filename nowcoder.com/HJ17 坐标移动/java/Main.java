import java.io.IOException;
import java.util.Scanner;

/**
 * todo 完成所有用例，非最佳时间
 */
class Main {

    public static void main(String[] args) throws IOException {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner sc) throws IOException {
        String line = sc.nextLine();
        String[] tokens = line.split(";");
        int x = 0,y = 0;
        for (String token : tokens) {
            if (token.isEmpty()) {
                continue;
            }
            int step;
            try {
                step = Integer.parseInt(token.substring(1));
            } catch (NumberFormatException e) {
                continue;
            }
            char flag = token.charAt(0);
            if (flag == 'A') {
                x -= step;
            } else if (flag == 'D') {
                x += step;
            } else if (flag == 'W') {
                y += step;
            } else if (flag == 'S') {
                y -= step;
            } else {
                continue;
            }
            
        }
        System.out.println(x + "," + y);
    }

}
