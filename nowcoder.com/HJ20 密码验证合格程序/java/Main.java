
/*
 * 密码要求:

  1.长度超过8位

  2.包括大小写字母.数字.其它符号,以上四种至少三种

  3.不能有长度大于2的包含公共元素的子串重复 （注：其他符号不含空格或换行）
  
  todo 完成所有用例，非最佳时间
 */
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

class Main {

    public static void main(String[] args) throws IOException {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner sc) throws IOException {
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.length() <= 8) {
                System.out.println("NG");
                continue;
            }
            int[] flags = new int[4];
            for (int i = 0; i < line.length(); i++) {
                char c = line.charAt(i);
                if (c >= '0' && c <= '9') {
                    flags[0] = 1;
                } else if (c >= 'a' && c <= 'z') {
                    flags[1] = 1;
                } else if (c >= 'A' && c <= 'Z') {
                    flags[2] = 1;
                } else if (c == ' ' && c == '\r') {
                    System.out.println("NG");
                    continue;
                } else {
                    flags[3] = 1;
                }
            }
            int sum = 0;
            for (int i = 0; i < flags.length; i++) {
                sum += flags[i];
            }
            if (sum < 3) {
                System.out.println("NG");
                continue;
            }
            if (chechR(line)) {
                System.out.println("NG");
                continue;
            }
            System.out.println("OK");
        }
    }

    private static boolean chechR(String line) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < line.length() - 2; i++) {
            String sub = line.substring(i, i + 3);
            if (set.contains(sub)) {
                return true;
            }
            set.add(sub);
        }
        return false;
    }
}
