import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * todo 需要优化
 */
class Main {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc.nextInt());
        }
    }

    private static void solution(int num) {
        if (num == 1) {
            System.out.println(num);
        }
        Map<Integer, Integer> map = new TreeMap<>();
        map.put(2,4);
        map.put(3,6);
        map.put(5,5);
        map.put(7,7);
        TreeMap<Integer, List<Integer>> treeMap = new TreeMap<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer value = entry.getValue();
            treeMap.computeIfAbsent(value,(k) -> new ArrayList<>()).add(entry.getKey());
        }
        for (int i = 8,s = i*i; num >= s;) {
            Map.Entry<Integer, List<Integer>> firstEntry = treeMap.firstEntry();
            Integer key = firstEntry.getKey();
            if (i > key) {
                List<Integer> primes = treeMap.remove(key);
                for (Integer prime : primes) {
                    int newValue = prime+key;
                    map.put(prime,newValue);
                    treeMap.computeIfAbsent(newValue,(k) -> new ArrayList<>()).add(prime);
                }
            } else {
                if (i < key) {
                    map.put(i,i);
                }
                i++;
                s = i*i;
            }
        }
        int n = num;
        List<Integer> primes = new ArrayList<>(map.keySet());
        for (int i = 0; i < primes.size(); ) {
            Integer prime = primes.get(i);
            if (n % prime == 0) {
                System.out.print(prime+" ");
                n = n/prime;
            } else {
                i++;
            }
        }
        if (n != 1) {
            System.out.println(n);
        }
    }
}
