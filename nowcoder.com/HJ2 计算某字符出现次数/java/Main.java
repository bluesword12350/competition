import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String l1 = in.nextLine().toLowerCase();
        char l2 = in.nextLine().toLowerCase().charAt(0);
        char[] c1 = l1.toCharArray();
        int length = 0;
        for (char c : c1) {
            if (c == l2) {
                length++;
            }
        }
        System.out.println(length);
    }
}