import java.util.Scanner;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        TreeSet<Integer> treeSet = new TreeSet<>();
        for (int i = 0; i < num; i++) {
            int n = sc.nextInt();
            treeSet.add(n);
        }
        for (Integer integer : treeSet) {
            System.out.println(integer);
        }
        sc.close();
    }

}
