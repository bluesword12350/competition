import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner sc) {
        String num = sc.next();
        char[] chars = num.toCharArray();
        Set<Character> set = new HashSet<>();
        StringBuilder builder = new StringBuilder();
        for (int i = chars.length - 1; i >= 0; i--) {
            char c = chars[i];
            if (set.contains(c)) {
                continue;
            }
            builder.append(c);
            set.add(c);
        }
        System.out.println(builder.toString());
    }

}
