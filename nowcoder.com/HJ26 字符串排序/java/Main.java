/*
 * todo 完成所有用例，非最佳时间
 */
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

class Main {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            solution(sc);
        }
    }

    private static void solution(Scanner sc) {
        String line = sc.nextLine();
        char[] chars = line.toCharArray();
        List<Character> zmList = new java.util.ArrayList<>();
        Map<Integer, Character> oMap = new TreeMap<>();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((c>='A'&& c<='Z') || (c>='a'&& c<='z')) {
                zmList.add(c);
            } else {
                oMap.put(i, c);
            }
        }
        zmList.sort(Comparator.comparingInt(Character::toUpperCase));
        StringBuilder builder = new StringBuilder();
        for (int i = 0,j = 0;j < zmList.size(); i++) {
            if (oMap.containsKey(i)) {
                builder.append(oMap.remove(i));
            } else {
                builder.append(zmList.get(j++));
            }
        }
        if (!oMap.isEmpty()){
            for (Map.Entry<Integer, Character> entry : oMap.entrySet()) {
                builder.append(entry.getValue());
            }
        }
        System.out.println(builder);
    }

}
