class Solution {
    public int totalHammingDistance(int[] nums) {
        int ans = 0 ,length = nums.length;
        for (int i=0;i<=30;i++) {
            int oneCount = 0;
            for (int num : nums) {
                oneCount += (num>>i) & 1;
            }
            ans += oneCount * (length - oneCount);
        }
        return ans;
    }
}