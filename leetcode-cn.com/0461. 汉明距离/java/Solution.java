class Solution {
    public int hammingDistance(int x, int y) {
        int xor = x ^ y;
        int result = 0;
        while(xor > 0){
            result += xor & 1;
            xor = xor >> 1;
        }
        return result;
    }
}

class Test {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.hammingDistance(1, 2));
    }
}