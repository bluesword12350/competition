class Solution {
    private int index;
    public String reverseParentheses(String s) {
        this.index = 0;
        return reverse(s.toCharArray()).toString();
    }

    private StringBuilder reverse(char[] ch) {
        StringBuilder res = new StringBuilder();
        while(index<ch.length) {
            char c = ch[index++];
            if(c == '(') {
                res.append(reverse(ch).reverse());
            } else if(c ==')'){
                break;
            } else {
                res.append(c);
            }
        }
        return res;
    }
}