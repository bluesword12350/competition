class Solution {
    public boolean[] canEat(int[] candiesCount, int[][] queries) {
        long[] target = new long[candiesCount.length+1];
        for (int i = 0; i < candiesCount.length; i++) {
            target[i+1] = target[i]+candiesCount[i];
        }
        var answer = new boolean[queries.length];
        for (int i = 0; i < queries.length; i++) {
            int favoriteType = queries[i][0],favoriteDay = queries[i][1],dailyCap = queries[i][2];
            long min = favoriteDay+1;
            long max = min * dailyCap;
            answer[i] = target[favoriteType+1] >= min && target[favoriteType]+1 <= max ;
        }
        return answer;
    }
}

class Test {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(java.util.Arrays.toString(solution.canEat(new int[]{5,2,6,4,1}, new int[][]{{3,1,2},{4,10,3},{3,10,100},{4,100,30},{1,3,1}})));
    }
}